import React, {Fragment} from 'react';
import Tags from './tags'
import Edit from '../Edit-Add/Edit'

class ListOfArticles extends React.Component{
    removeHandle = () =>{
        const id = this.props.details.id;
        this.props.remove(id);
    };

    render(){
        const tagsArr = this.props.details.tags;
        return (
            <Fragment>
                    <div className={'article border border-primary rounded'} style={{margin: 40, padding: 15}}>
                        <div className={'title'}>
                            <h3>{this.props.details.title}</h3>
                        </div>
                        <div className={'info'}>
                            <p>{this.props.details.body}</p>
                        </div>
                        <div className={'tags'}>
                            {tagsArr.map( key =>( <Tags key={key} data={key}  />))}
                        </div>
                        <div className='control' style={{marginBottom:20}}>
                            <button  className="btn btn-danger btn-mini" style={{margin:10}} onClick={this.removeHandle} >Удалить</button>
                            <button  className="btn btn-danger btn-mini"
                                     onClick={ ()=>{document.getElementById(this.props.index).style.display = 'inline';}}>Редактировать</button>
                        </div>
                        <div id={this.props.index} style={{display:'none'}}>
                            <Edit artid={this.props.details.id} index={this.props.index} />
                        </div>
                    </div>
            </Fragment>
        );
    }
}

export default ListOfArticles;