import React from 'react';


class Add extends React.Component{

    titleRef = React.createRef();
    bodyRef = React.createRef();
    tagsRef = React.createRef();

    createArticle = (e) => {

        const Article = {
            id:Date.now(),
            title: this.titleRef.value.value,
            body: this.bodyRef.value.value,
            tags: this.tagsRef.value.value.split(", "),
        };
        let articles = localStorage.getItem("Articles");
        articles = JSON.parse(articles);
        articles.push(Article);
        localStorage.setItem("Articles",JSON.stringify(articles))
    };


    render(){

        return (
            <React-Fragment>
                <form className='Add-article form-group col-sm-12'
                      onSubmit={this.createArticle}>
                    <div className="form-group">
                        <input className="form-control"
                               name="title" ref ={this.titleRef } type = "text"
                               placeholder='Заголовок'/>
                    </div>
                    <div className="form-group">
                        <textarea  className={"form-control"}
                                   name="body" ref ={this.bodyRef }
                                   placeholder='Твои мысли'
                                   rows="4"/>
                    </div>
                    <div className="form-group">
                        <input  className="form-control"
                                name="tags" ref ={this.tagsRef }
                                type = "text"
                                placeholder='Тэги, И еще тэг, Ну может еще 1'/>
                    </div>
                    <button className={"btn btn-primary"}
                            type='submit'>Добавить</button>
                </form>
            </React-Fragment>
        );
    }
}

export default Add;


