import React, {Fragment} from 'react';
import ListOfArticles from "./List-of-Articles/List-of-Articles";
import data from './Data/posts.json'
import Add from "./Edit-Add/Add";

class App extends React.Component {

    remove = (id) => {
        let articles = localStorage.getItem("Articles");
        articles = JSON.parse(articles);
        for(let i in articles){
            if(articles[i].id === id){
                articles.splice(i,1);
                localStorage.setItem("Articles",JSON.stringify(articles));
                window.location.reload()
            }
        }
    };

    render() {

        let articles = localStorage.getItem("Articles");
        if(articles === null || articles.length === 2){
            localStorage.setItem("Articles",JSON.stringify(data));
            articles = data;
        }else{
            articles = JSON.parse(articles);
        }

        return (
          <Fragment>
              <div className="container">
                  <div className="row">
                      <div className="col-sm-7" style={{marginTop: 50}}>
                          {Object.keys(articles).map(key => (
                              <ListOfArticles remove = {this.remove}  key={key} index={key} details={articles[key]} />
                          ))}
                      </div>
                      <div className="col">
                          <div className="col-sm-12" style={{marginTop:90}}>
                            <Add/>
                          </div>
                      </div>
                  </div>
              </div>
          </Fragment>
        );
    }
}

export default App;


